package mongo4cj.mongo

import std.collection.*
import mongo4cj.bson.*

public class Client{

    private var client: CPointer<mongo_client_t> = CPointer<mongo_client_t>()

    public init(uri: String){
        unsafe {
            mongoc_init()
            var uriStr = LibC.mallocCString(uri)
            this.client = mongoc_client_new(uriStr)
            LibC.free(uriStr)
        }
    }   
    ~init(){
        unsafe{
            mongoc_client_destroy(this.client)
            mongoc_cleanup()
        }
    }
    public operator func[](db:String): DataBase {
        return DataBase(this.client, db)
    }

    public func Command_simple(db: String, command: Array<kvb>): Option<Bson>{
        return this.command_simple(db, Bson(command))
    }
    func command_simple(db: String, command: Bson): Option<Bson>{
        unsafe {
            var reply = Bson()
            var dbStr = LibC.mallocCString(db)
            var readNull  = CPointer<Unit>()
            var errPtr = LibC.malloc<bson_error_t>()
            var ret = mongoc_client_command_simple(this.client, dbStr, command.b, readNull, reply.b, errPtr)
            if (ret) {
                LibC.free(errPtr)
                return None
            }
            var err = checkErr(errPtr)
            if (err.isNone()){
                return reply
            }
            return None
        }
    }
}


public class DataBase{

    private var client: CPointer<mongo_client_t> = CPointer<mongo_client_t>()

    private var db: String = ""

    internal init(client: CPointer<mongo_client_t>, db: String){
        this.client = client
        this.db = db
    }

    public operator func[](collection:String): Collection {
        return Collection(this.client, this.db, collection)
    }
}

public class Collection{

    private var client: CPointer<mongo_client_t> = CPointer<mongo_client_t>()

    private var collection: CPointer<mongoc_collection_t> = CPointer<mongoc_collection_t>()

    internal init(client: CPointer<mongo_client_t>, db: String, collection: String){
        unsafe {
            this.client = client
            var dbStr = LibC.mallocCString(db)
            var tableStr = LibC.mallocCString(collection)
            this.collection = mongoc_client_get_collection(client, dbStr, tableStr)
            LibC.free(dbStr)
            LibC.free(tableStr)
        }
    }
    ~init(){
        unsafe{
            mongoc_collection_destroy(this.collection)
        }
    }

    public func InsertOne(doc: Array<kvb>): Option<Exception>{
        return this.insertOne(Bson(doc))
    }

    func insertOne(doc: Bson): Option<Exception>{
        unsafe {
            var optsNull = CPointer<bson_t>()
            var replyNull  = CPointer<bson_t>()
            var errPtr = LibC.malloc<bson_error_t>()

            var ret = mongoc_collection_insert_one(collection, doc.b, optsNull, replyNull, errPtr)
            if (ret){
                LibC.free(errPtr)
                return None
            }
            return checkErr(errPtr)
        }
    }
    public func InsertMany(docs: ArrayList<kvb>): Option<Exception>{
        var rows =  ArrayList<Bson>()
        for(doc in docs){
            rows.append(Bson(doc))
        }
        return this.insertMany(rows)
    }
    func insertMany(docs: ArrayList<Bson>): Option<Exception>{
        unsafe {
            var optsNull = CPointer<bson_t>()
            var replyNull  = CPointer<bson_t>()
            var errPtr = LibC.malloc<bson_error_t>()

            var rows = LibC.malloc<CPointer<bson_t>>()
            var i = 0
            while(i < docs.size){
                rows.write(i, docs[i].b)
                i++
            }
            var ret = mongoc_collection_insert_many(collection, rows, IntNative(docs.size), optsNull, replyNull, errPtr)
            if (ret){
                LibC.free(errPtr)
                return None
            }
            return checkErr(errPtr)
        }
    }
    public func Delete(query: Array<kvb>): Option<Exception>{
        return this.delete(Bson(query))
    }
    func delete(query: Bson): Option<Exception>{
        unsafe {
            var optsNull = CPointer<bson_t>()
            var replyNull  = CPointer<bson_t>()
            var errPtr = LibC.malloc<bson_error_t>()

            var ret = mongoc_collection_delete_one(collection, query.b, optsNull, replyNull, errPtr)
            if (ret){
                LibC.free(errPtr)
                return None
            }
            return checkErr(errPtr)
        }
    }

    public func Update(query: Array<kvb>, update: Array<kvb>): Option<Exception>{
        return this.update(Bson(query), Bson(update))
    }
    func update(query: Bson, update: Bson): Option<Exception>{
        unsafe {
            var optsNull = CPointer<bson_t>()
            var replyNull  = CPointer<bson_t>()
            var errPtr = LibC.malloc<bson_error_t>()

            var ret = mongoc_collection_update_one(collection, query.b, update.b, optsNull, replyNull, errPtr)
            if (ret){
                LibC.free(errPtr)
                return None
            }
            return checkErr(errPtr)
        }
    }

    public func Find(query!: Array<kvb>=[], skip!: UInt32=0, limit!: UInt32=0, fields!: Array<kvb> = []): ArrayList<Bson>{
        return this.find(Bson(query), skip, limit, Bson(fields))
    }

    func find(query: Bson, skip: UInt32, limit: UInt32, fields: Bson): ArrayList<Bson>{
        unsafe {

            var readNull  = CPointer<Unit>()
            
            var cursor = mongoc_collection_find(collection, MONGOC_QUERY_NONE, skip, limit, 0, query.b, fields.b, readNull)

            var rows = ArrayList<Bson>()
            var ret = LibC.malloc<bson_t>()
            while(mongoc_cursor_next(cursor, inout ret)){
                rows.append(Bson(ret))
            }
            mongoc_cursor_destroy(cursor)

            return rows
        }
    }

    public func Count(query!: Array<kvb>=[], skip!: Int64=0, limit!: Int64=0): Option<Int64>{
        return this.count(Bson(query), skip, limit)
    }
    func count(query: Bson, skip: Int64, limit: Int64): Option<Int64>{
        unsafe {

            var readNull  = CPointer<Unit>()
            var errPtr = LibC.malloc<bson_error_t>()

            var count = mongoc_collection_count(collection, MONGOC_QUERY_NONE, query.b, skip, limit, readNull, errPtr)
           
            var err = checkErr(errPtr)
            if (err.isNone()){
                return count
            }
            return None
        }
    }
    public func Command_simple(command: Array<kvb>): Option<Bson>{
        return this.command_simple(Bson(command))
    }
    func command_simple(command: Bson): Option<Bson>{
        unsafe {
            var reply = Bson()
            var readNull  = CPointer<Unit>()
            var errPtr = LibC.malloc<bson_error_t>()
            var ret = mongoc_collection_command_simple(this.collection, command.b, readNull, reply.b, errPtr)
            if (ret) {
                LibC.free(errPtr)
                return reply
            }
           var err = checkErr(errPtr)
           if (err.isNone()){
                return reply
           }
           return None
        }
    }
    // func find_with_opts(query: Bson): ArrayList<Bson>{
    //     unsafe {
    //         var optsNull = CPointer<bson_t>()
    //         var readNull  = CPointer<Unit>()

    //         var cursor = mongoc_collection_find_with_opts(collection, query.b, optsNull, readNull)
    
    //         var rows = ArrayList<Bson>()
    //         var ret = LibC.malloc<bson_t>()
    //         while(mongoc_cursor_next(cursor, inout ret)){
    //             rows.append(Bson(ret))
    //         }
    //         mongoc_cursor_destroy(cursor)

    //         return rows
    //     }
    // }

}

extend Collection{

    public func Select<T>(query!: Array<kvb>=[], skip!: UInt32=0, limit!: UInt32=0, fields!: Array<kvb> = []): ArrayList<T> where T <: IModel<T> {
        return this.select<T>(Bson(query), skip, limit, Bson(fields))
    }
    
    func select<T>(query: Bson, skip: UInt32, limit: UInt32, fields: Bson):  ArrayList<T> where T <: IModel<T> {

        let list = ArrayList<T>()
        unsafe {
            let obj = zeroValue<T>()

            var rows = this.find(query, skip, limit, fields)
            for(val in rows){
                var node = val.parser()
                list.append(obj.decode(node))
            }
            return list
        }
    }

    public func SaveOne<T>(doc: T): Option<Exception> where T <: IModel<T>{
        var obj = doc.encode()
        return this.insertOne(Bson(obj))
    }   

    public func SaveMany<T>(docs: ArrayList<T>): Option<Exception> where T <: IModel<T>{
        var objs = ArrayList<Bson>()
        for (doc in docs) {
            var obj = doc.encode()
            objs.append(Bson(obj))
        }
        return this.insertMany(objs)
    } 
}


