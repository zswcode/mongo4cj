# mongo4cj

https://gitcode.com/zswcode/mongo4cj.git

#### 介绍

##### 使用仓颉调用mongodb
###### 注意:仅ubuntu下跑过, 其他平台请先自行编译动态链接库
###### 需安装openssl, libz 等库

使用示例

```
    @Model
    class User{
        var _id: ObjectId = ObjectId()
        var name: String = ""
        var sex: Int64 = 0
        var age: DateTime = DateTime.now()
    }


    var client = Client("mongodb://root:Admin123Abc123@10.10.10.13:27017")

    var db = client["rescue_girl"]

    var table = db["demo"]

    table.Insert([
        kvb("_id",ObjectId()),
        kvb("name", "xyz"),
        kvb("sex",123),
        kvb("age", DateTime.now())
    ])

    var u1 = User()
    table.InsertOne(u1)

    var ret1 = table.Delete([
        kvb("name","xyz")
    ])

    table.Update([
        kvb("name","xyz")
    ],[
        kvb("$set", kvb(
            "sex", 2,
        ))
    ])
 
    var ret = table.Find(query: [], skip: 0, limit: 1, fields: [
        kvb("_id", false),
        kvb("name",1)
    ])

    var rows = table.Select<User>(query: [], skip: 0, limit: 1)
    for(u1 in rows){
        println(u1.name)
    }

    var countOpt = table.Count()

    var cmd = table.Command_simple([
        kvb("collStats", "demo")
    ])
```